---
layout: home

hero:
  name: "Sunny Attic Software"
  text: "Clear and Bright Software in Spain"
  tagline: Domain Drive Design, CQRS, Event Sourcing, etc.
---

<CardContainer/>