---
title: Taurus MyCook no enciende o se bloquea
date: 2023-09-05
description: Cómo arreglar una Taurus MyCook Touch que se queda atascada en la pantalla inicial o se bloquea
---

## Introducción
La Taurus MyCook Touch tiene un dispositivo/tablet Android con un puerto usb micro B.

Para acceder a este dispositivo, hacer un soft reset o, incluso, conectarse por USB, lo primero que se debe hacer es quitar todos los tornillos de la base. Sigue los siguientes pasos.

## Pasos

## Paso 1 - Quitar tornillos
Bloquea la báscula moviendo la pestaña a OFF y apretando el tornillo de la báscula hasta que haga tope (sigue las instrucciones de la pegatina que deberías tener en la base)
Después quita todos los tornillos de la base de la Taurus. Presta atención a uno que está escondido tras una pegatina negra no visible a simple vista.
Extrae la carcasa con cuidado, sin tensar demasiado los cables que van de la placa base al dispositivo táctil Android. No hace falta que quites los tornillos del dispositivo Android que lo une a la carcasa.

## Paso 2 - Soft reset
Mantén pulsado durante un rato el botón de encendido del dispositivo Android hasta que reinicie. Si esto no soluciona el problema, continúa.

## Paso 3 - Acceder al Recovery Mode
Apaga la Taurus, cuando la enciendas mantén pulsado el botón de encendido del dispositivo Android a la vez que el botón de volumen más pegado al cable de alimentación. El dispositivo debería entrar en modo "recovery" y mostrar una serie de opciones. Si puedes navegar con los botones de volumen y seleccionar opciones con el botón de encendido, podrás hacer un reseteo de fábrica. Si esto no soluciona el problema, o si no puedes navegar por las opciones, continúa.

## Paso 4 - Gestiona el dispositivo desde un PC externo
Instala ADB para Windows, Linux o Mac. Puedes encontrar los enlaces en este [artículo](https://www.xatakandroid.com/tutoriales/como-conectar-movil-android-al-ordenador-adb). Vamos a asumir que estás usando Windows.

Cuando descargues el zip del ADB para Windows, descomprímelo y abre una terminal CMD en ese directorio. Deberías poder ejecutar un comando `adb` sin problemas.

Desenchufa el cable de usb del dispositivo y enchufa un cable USB (necesitas uno que tenga conexión macho al PC y USB micro B). Enciende la Taurus y ejecuta
```
adb devices
```
Si todo va bien, el PC reconocerá el dispositivo USB y adb reconocerá el dispositivo, mostrando algo como 
```
List of devices attached
0123456789ABCDEF        recovery
```

Si muestra *recovery* es porque el dispositivo Android está en modo recovery. No podrás hacer mucho en este caso, porque ADB necesita que el sistema operativo cargue mínimamente.

Si, por el contrario, el sistema operativo ha cargado, o la Taurus está atascada en la pantalla inicial que muestra el logo y se queda cargando indefinidamente, el comando de `adb devices` debería también mostrar el dispositivo encontrado.

Ejecuta
```
adb shell
```
Ya estás dentro del dispositivo, en una terminal Linux estándar (Android es Linux!) y puedes hacer prácticamente lo que quieras.

Podrías, por ejemplo, forzar un factory reset con
```
wipe data
```
o con 
```
adb shell recovery --wipe_data
```
y después hacer un `reboot`

Esto debería arreglar la mayoría de los problemas.