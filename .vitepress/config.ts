import { defineConfig } from 'vitepress'

// https://vitepress.dev/reference/site-config
export default defineConfig({
  title: "Sunny Attic Software",
  description: "Software development in Spain",
  themeConfig: {
    logo: '/sasw-logo-web.png',
    siteTitle: false,

    // https://vitepress.dev/reference/default-theme-config
    nav: [
      { text: 'Home', link: '/' },
    ],

    socialLinks: [
      { icon: 'github', link: 'https://github.com/sunnyatticsoftware' }
    ],

    search: {
      provider: 'local'
    }
  }
})
