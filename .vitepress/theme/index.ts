// https://vitepress.dev/guide/custom-theme
import { h } from 'vue'
import Theme from 'vitepress/theme'
import './style.css'
import './css/custom.css'
import ArticleCard from './components/ArticleCard.vue';
import CardContainer from './components/CardContainer.vue';

export default {
  extends: Theme,
  Layout: () => {
    return h(Theme.Layout, null, {
      // https://vitepress.dev/guide/extending-default-theme#layout-slots
    })
  },
  enhanceApp({ app, router, siteData }) {
    app.component('ArticleCard', ArticleCard);
    app.component('CardContainer', CardContainer);
  }
}